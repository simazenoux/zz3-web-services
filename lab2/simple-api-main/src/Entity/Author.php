<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use EasyApiBundle\Entity\AbstractBaseEntity;
use Symfony\Component\Serializer\Annotation\MaxDepth;


/**
 * @ORM\Entity()
 */
class Author extends AbstractBaseEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue
     */
    protected ?int $id = null;

    /**
     * @ORM\Column(type="string")
     */
    protected ?string $firstName = null;

    /**
     * @ORM\Column(type="string")
     */
    protected ?string $lastName = null;


    /**
     * MaxDepth(1)
     * @ORM\OneToMany(targetEntity="Book", mappedBy="author")
     */
    protected ?Collection $books = null;


    public function __construct()
    {
        $this->books = new ArrayCollection();
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string|null $firstName
     */
    public function setFirstName(?string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string|null $lastName
     */
    public function setLastName(?string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return Collection|Product[]
     */
    public function getBooks(): Collection
    {
        return $this->books;
    }

    /**
     * @param Book|null $book
     */
    public function addBook(?Book $book): void
    {
        if (!$this->books->contains($book)) {
            $this->books->add($book);
        }
    }

    /**
     * @param Book|null $book
     */
    public function removeBook(?Book $book): void
    {
        if ($this->books->contains($book)) {
            $this->products->removeElement($book);
            if ($book->getAuthor() === $this) {
                $book->setAuthor(null);
            }
        }
    }
}